#import <Foundation/Foundation.h>

#if defined __cplusplus
extern "C" {
#endif  // __cplusplus

NS_ASSUME_NONNULL_BEGIN

/**
 * This enum specifies the languages that are supported by `Translator`.
 *
 * Note that if and when languages are added / removed, we would like to keep the existing
 * enum values - hence specifying exactly even if they match defaults.
 *
 * Next available enum = 59.
 */
typedef NS_ENUM(NSUInteger, FIRTranslateLanguage) {
  /* Afrikaans. */ FIRTranslateLanguageAF = 0,
  /* Arabic. */ FIRTranslateLanguageAR = 1,
  /* Belarusian. */ FIRTranslateLanguageBE = 2,
  /* Bulgarian. */ FIRTranslateLanguageBG = 3,
  /* Bengali. */ FIRTranslateLanguageBN = 4,
  /* Catalan. */ FIRTranslateLanguageCA = 5,
  /* Czech. */ FIRTranslateLanguageCS = 6,
  /* Welsh. */ FIRTranslateLanguageCY = 7,
  /* Danish. */ FIRTranslateLanguageDA = 8,
  /* German. */ FIRTranslateLanguageDE = 9,
  /* Greek. */ FIRTranslateLanguageEL = 10,
  /* English. */ FIRTranslateLanguageEN = 11,
  /* Esperanto. */ FIRTranslateLanguageEO = 12,
  /* Spanish. */ FIRTranslateLanguageES = 13,
  /* Estonian. */ FIRTranslateLanguageET = 14,
  /* Persian. */ FIRTranslateLanguageFA = 15,
  /* Finnish. */ FIRTranslateLanguageFI = 16,
  /* French. */ FIRTranslateLanguageFR = 17,
  /* Irish. */ FIRTranslateLanguageGA = 18,
  /* Galician. */ FIRTranslateLanguageGL = 19,
  /* Gujarati. */ FIRTranslateLanguageGU = 20,
  /* Hebrew. */ FIRTranslateLanguageHE = 21,
  /* Hindi. */ FIRTranslateLanguageHI = 22,
  /* Croatian. */ FIRTranslateLanguageHR = 23,
  /* Haitian. */ FIRTranslateLanguageHT = 24,
  /* Hungarian. */ FIRTranslateLanguageHU = 25,
  /* Indonesian. */ FIRTranslateLanguageID = 26,
  /* Icelandic. */ FIRTranslateLanguageIS = 27,
  /* Italian. */ FIRTranslateLanguageIT = 28,
  /* Japanese. */ FIRTranslateLanguageJA = 29,
  /* Georgian. */ FIRTranslateLanguageKA = 30,
  /* Kannada. */ FIRTranslateLanguageKN = 31,
  /* Korean. */ FIRTranslateLanguageKO = 32,
  /* Lithuanian. */ FIRTranslateLanguageLT = 33,
  /* Latvian. */ FIRTranslateLanguageLV = 34,
  /* Macedonian. */ FIRTranslateLanguageMK = 35,
  /* Marathi. */ FIRTranslateLanguageMR = 36,
  /* Malay. */ FIRTranslateLanguageMS = 37,
  /* Maltese. */ FIRTranslateLanguageMT = 38,
  /* Dutch. */ FIRTranslateLanguageNL = 39,
  /* Norwegian. */ FIRTranslateLanguageNO = 40,
  /* Polish. */ FIRTranslateLanguagePL = 41,
  /* Portuguese. */ FIRTranslateLanguagePT = 42,
  /* Romanian. */ FIRTranslateLanguageRO = 43,
  /* Russian. */ FIRTranslateLanguageRU = 44,
  /* Slovak. */ FIRTranslateLanguageSK = 45,
  /* Slovenian. */ FIRTranslateLanguageSL = 46,
  /* Albanian. */ FIRTranslateLanguageSQ = 47,
  /* Swedish. */ FIRTranslateLanguageSV = 48,
  /* Swahili. */ FIRTranslateLanguageSW = 49,
  /* Tamil. */ FIRTranslateLanguageTA = 50,
  /* Telugu. */ FIRTranslateLanguageTE = 51,
  /* Thai. */ FIRTranslateLanguageTH = 52,
  /* Tagalog. */ FIRTranslateLanguageTL = 53,
  /* Turkish. */ FIRTranslateLanguageTR = 54,
  /* Ukranian. */ FIRTranslateLanguageUK = 55,
  /* Urdu. */ FIRTranslateLanguageUR = 56,
  /* Vietnamese. */ FIRTranslateLanguageVI = 57,
  /* Chinese. */ FIRTranslateLanguageZH = 58,
  /* Unsupported or invalid language */ FIRTranslateLanguageInvalid = 0xffff,
} NS_SWIFT_NAME(TranslateLanguage);

/** Returns the BCP-47 language code for the given `TranslateLanguage`. */
NSString* FIRTranslateLanguageCodeForLanguage(FIRTranslateLanguage language)
    NS_SWIFT_NAME(TranslateLanguage.toLanguageCode(self:));

/**
 * Returns the `TranslateLanguage` for a given BCP-47 language code, or
 * `TranslateLanguage.Invalid` if the language code is invalid or not supported
 * by the Translate API.
 */
FIRTranslateLanguage FIRTranslateLanguageForLanguageCode(NSString* languageCode)
    NS_SWIFT_NAME(TranslateLanguage.fromLanguageCode(_:));

/**
 * Returns a set that contains `TranslateLanguage` codes of all languages supported by the
 * translate API.
 */
NSSet<NSNumber*>* FIRTranslateAllLanguages(void) NS_SWIFT_NAME(TranslateLanguage.allLanguages());

NS_ASSUME_NONNULL_END

#if defined __cplusplus
}
#endif  // __cplusplus
