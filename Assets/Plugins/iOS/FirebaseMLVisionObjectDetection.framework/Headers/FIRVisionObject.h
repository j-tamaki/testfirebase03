#import <CoreGraphics/CoreGraphics.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * @options VisionObjectCategory
 * Categories of objects returned by the detector.
 */
typedef NS_ENUM(NSInteger, FIRVisionObjectCategory) {
  /** An unknown object category. All objects that are not associated with a category. */
  FIRVisionObjectCategoryUnknown,
  /** The Home Goods object category. */
  FIRVisionObjectCategoryHomeGoods,
  /** The Fashion Goods object category. */
  FIRVisionObjectCategoryFashionGoods,
  /** The Food object category. */
  FIRVisionObjectCategoryFood,
  /** The Places object category. */
  FIRVisionObjectCategoryPlaces,
  /** The Plants object category. */
  FIRVisionObjectCategoryPlants,
} NS_SWIFT_NAME(VisionObjectCategory);

/** An object detected in an image. */
NS_SWIFT_NAME(VisionObject)
@interface FIRVisionObject : NSObject

/**
 * The rectangle that holds the discovered object relative to the detected image in the view's
 * coordinate system.
 */
@property(nonatomic, readonly) CGRect frame;

/**
 * The category of the object returned by the classifier. The property returns `.unknown` if the
 * detector option `shouldEnableClassification` is set to `NO`.
 */
@property(nonatomic, readonly) FIRVisionObjectCategory classificationCategory;

/**
 * The tracking identifier of the vision object. The value is a non-negative `integerValue`. The
 * value is `nil` if no tracking ID was provided.
 */
@property(nonatomic, readonly, nullable) NSNumber *trackingID;

/**
 * Opaque entity ID used to query the [Google Knowledge Graph Search API]
 * (https://developers.google.com/knowledge-graph/) to get a localized description of the label. For
 * example: ["/g/11g0srrsqr"](https://hume.google.com/edit/g/11g0srrsqr?q=%2Fg%2F11g0srrsqr)
 * represents "Fashion good". If the object is not classified, the value returned is a [generic
 * object] (https://hume.google.com/edit/m/0bl9f) category in the graph. The value is `nil` if the
 * entity ID was not provided.
 */
@property(nonatomic, copy, readonly, nullable) NSString *entityID;

/**
 * The label associated with the object, i.e. human readable string in American English. For
 * example: "Fashion good". The value is `nil` if the label was not provided.
 *
 * Note: this is not fit for display purposes, as it is not localized. Use `entityID` and query the
 * Knowledge Graph to get a localized description of the label.
 */
@property(nonatomic, copy, readonly, nullable) NSString *label;

/**
 * The probability confidence for the vision object in range from 0 to 1. The value is a
 * `floatValue`. The value is `nil` if the object is detected, but a valid confidence was not
 * provided.
 */
@property(nonatomic, readonly, nullable) NSNumber *confidence;

/** Unavailable. */
- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
